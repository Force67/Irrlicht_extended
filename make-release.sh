#!/bin/bash

#if you want to change irrlicht_extended and build e.g. in debug mode 
#you should do the build steps yourself

OS="$(uname -m)"
target=""
if [ "$OS" = "x86_64" ]; then
	target="64"
else
	target="32"
fi

PARALLEL_JOBS=`nproc 2>/dev/null`
if [ "x$?" != "x0" ]; then
    echo "Couldn't determine # of available processors using 'nproc' -> using default (1)"
    PARALLEL_JOBS=1
fi
[ -z "$PARALLEL_JOBS" ] && PARALLEL_JOBS=1

chmod +x bx/tools/bin/linux/genie
set -x
cd bgfx
make clean # in case of new files need to be generated
make linux-release"$target" -j "$PARALLEL_JOBS" || exit 2
make linux-debug"$target" -j "$PARALLEL_JOBS" || exit 2

cd ..
mkdir lib/Linux
cp bgfx/.build/linux"$target"_gcc/bin/* lib/Linux/

cd source/Irrlicht/
make clean
make NDEBUG=1 -j "$PARALLEL_JOBS" || exit 2
# must call 'make clean' inbetween because all files are compiled in-source
# and all .o files would still exist otherwise
make clean
make -j "$PARALLEL_JOBS" || exit 2
