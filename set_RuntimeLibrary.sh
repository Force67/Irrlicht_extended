#!/bin/bash
OLDDebug="MultiThreadedDebug<"
NEWDebug="MultiThreadedDebugDLL<"
OLDRelease="MultiThreaded<"
NEWRelease="MultiThreadedDLL<"
DPATH="bgfx/.build/projects/vs2015/*.vcxproj"
BPATH="bgfx/backupVS"
TFILE="/tmp/out.tmp.$$"
for f in $DPATH
do
  if [ -f $f -a -r $f ]; then
   sed -i "s/$OLDDebug/$NEWDebug/g" "$f"
   sed -i "s/$OLDRelease/$NEWRelease/g" "$f"
  else
   echo "Error: Cannot read $f"
  fi
done
