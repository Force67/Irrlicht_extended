// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BGFX_CACHE_HANDLER_H_INCLUDED__
#define __C_BGFX_CACHE_HANDLER_H_INCLUDED__

#include "IrrCompileConfig.h"

#ifdef _IRR_COMPILE_WITH_BGFX_

#include <bgfx/bgfx.h>

#include "SMaterial.h"
#include "ITexture.h"
#include <iostream>
#include <stack>

namespace irr
{
namespace video
{

template <class TBgfxDriver, class TBgfxTexture>
class CBgfxCoreCacheHandler
{
	// all bgfx flags have the same type
	typedef  uint64_t bgfxFlag_t;

	class STextureCache
	{
	public:
		STextureCache(CBgfxCoreCacheHandler* cacheHandler, u32 textureCount) :
			CacheHandler(cacheHandler), DriverType(cacheHandler->getDriverType()), TextureCount(textureCount)
		{
			for (u32 i = 0; i < MATERIAL_MAX_TEXTURES; ++i)
			{
				Texture[i] = 0;
				core::stringc tempStr = "texture";
				tempStr.append(core::stringc(i).c_str());
				UniformHandles[i] = bgfx::createUniform(tempStr.c_str(),bgfx::UniformType::Int1);
			}
		}

		~STextureCache()
		{
			//clear();
		}

		const TBgfxTexture* operator[](int index) const
		{
			if (static_cast<u32>(index) < MATERIAL_MAX_TEXTURES)
				return Texture[static_cast<u32>(index)];

			return 0;
		}

		const TBgfxTexture* get(u32 index) const
		{
			if (index < MATERIAL_MAX_TEXTURES)
				return Texture[index];

			return 0;
		}


		bool set(u32 index, const ITexture* texture,
				const u8 wrapU = ETC_REPEAT, const u8 wrapV = ETC_REPEAT, const u8 wrapW = ETC_REPEAT, 
				const E_BGFX_TEXTURE_COMPARE_FLAGS compareFlag = E_BGFX_TEXTURE_COMPARE_NONE,
				const bool linearFilter = true)
		{
			bool status = false;
			u32 bgfxFlag= 0; //ETC_REPEAT

			if (compareFlag != E_BGFX_TEXTURE_COMPARE_NONE)
			{
				bgfxFlag |= IrrToBgfx::getBgfxCompareFlag(compareFlag);
			}
			
			bgfxFlag |= (linearFilter ? 0 : (BGFX_TEXTURE_MIN_POINT | BGFX_TEXTURE_MAG_POINT));

			if((wrapU != ETC_REPEAT)|| (wrapV != ETC_REPEAT) || (wrapW != ETC_REPEAT))
			{
				bgfxFlag |=	IrrToBgfx::getBgfxClampModeFromIrrlichtClampMode(0,wrapU)
							|IrrToBgfx::getBgfxClampModeFromIrrlichtClampMode(1,wrapV)
							|IrrToBgfx::getBgfxClampModeFromIrrlichtClampMode(2,wrapW);
			}
		
			E_DRIVER_TYPE type = DriverType;

			if (index < MATERIAL_MAX_TEXTURES && index < TextureCount)
			{
				//CacheHandler->setActiveTexture();

				const TBgfxTexture* prevTexture = Texture[index];
				if (DriverType == EDT_BGFX_D3D11 || DriverType == EDT_BGFX_D3D11)
				{
					setBgfxTexture(index, texture, bgfxFlag);
					
					if (prevTexture)
						prevTexture->drop();

				}
				else
				{
					if (texture != prevTexture)
					{
						setBgfxTexture(index, texture, bgfxFlag);

						if (prevTexture)
							prevTexture->drop();
					}
				}
				//if (!texture && prevTexture)
				//{
					//is there something we need to do?
				//}

				Texture[index] = static_cast<const TBgfxTexture*>(texture);


				status = true;
			}

			return (status && type == DriverType);
		}

		void remove(ITexture* texture)
		{
			if (!texture)
				return;

			for (u32 i = 0; i < MATERIAL_MAX_TEXTURES; ++i)
			{
				if (Texture[i] == texture)
				{
					Texture[i] = 0;

					texture->drop();
				}
			}
		}

		void clear()
		{
			for (u32 i = 0; i < MATERIAL_MAX_TEXTURES; ++i)
			{
				if (Texture[i])
				{
					const TBgfxTexture* prevTexture = Texture[i];

					Texture[i] = 0;

					prevTexture->drop();
				}
				if(bgfx::isValid(UniformHandles[i]))
                    bgfx::destroy(UniformHandles[i]);
			}
		}

	private:

		void setBgfxTexture(u32 idx, const ITexture* texture, u32 flag)
		{
			if (texture)
			{
				E_DRIVER_TYPE type = texture->getDriverType();

				if (type == DriverType)
				{
					texture->grab();

					const TBgfxTexture* curTexture = static_cast<const TBgfxTexture*>(texture);

					bgfx::setTexture(idx, UniformHandles[idx], curTexture->getTextureHandle(), flag);

				}
				else
				{
					texture = nullptr;

					os::Printer::log("Fatal Error: Tried to set a texture not owned by this driver.", ELL_ERROR);
				}
			}
		}

		CBgfxCoreCacheHandler* CacheHandler;

		E_DRIVER_TYPE DriverType;

		const TBgfxTexture* Texture[MATERIAL_MAX_TEXTURES];
		bgfx::UniformHandle UniformHandles[MATERIAL_MAX_TEXTURES];

		u32 TextureCount;
	};

public:
	CBgfxCoreCacheHandler(TBgfxDriver* driver) :
		Driver(driver), TextureCache(STextureCache(this, Driver->getFeature().TextureUnit)), FrameBufferCount(0),
		BlendEquation(0), BlendFunc(0), BlendSourceRGB(0), BlendDestinationRGB(0), BlendSourceAlpha(0), BlendDestinationAlpha(0),
		Blend(0), ColorMask(0), DepthFunc(BGFX_STATE_DEPTH_TEST_LESS), DepthMask(true),
		DepthTest(true), FrameBufferID(0), ProgramID(0), ActiveTexture(0), ViewportX(0), ViewportY(0)
	{
		const CBgfxCoreFeature& feature = Driver->getFeature();

		FrameBufferCount = core::max_(static_cast<u32>(1), static_cast<u32>(feature.MultipleRenderTarget));

		BlendEquation = new bgfxFlag_t[FrameBufferCount];
		BlendFunc = new bgfxFlag_t[FrameBufferCount];
		BlendSourceRGB = new bgfxFlag_t[FrameBufferCount];
		BlendDestinationRGB = new bgfxFlag_t[FrameBufferCount];
		BlendSourceAlpha = new bgfxFlag_t[FrameBufferCount];
		BlendDestinationAlpha = new bgfxFlag_t[FrameBufferCount];
		Blend = new bool[FrameBufferCount];

		ColorMask = new bool[FrameBufferCount][4];

		CullFace = new bool[FrameBufferCount];
		CullFaceMode = new bgfxFlag_t[FrameBufferCount];

		// Initial Bgfx values from specification.

		for (u32 i = 0; i < FrameBufferCount; ++i)
		{
			BlendEquation[i] = BGFX_STATE_BLEND_EQUATION(BGFX_STATE_BLEND_EQUATION_ADD);
			BlendFunc[i] = BGFX_STATE_BLEND_FUNC(BGFX_STATE_BLEND_ONE, BGFX_STATE_BLEND_ZERO);
			BlendSourceRGB[i] = BGFX_STATE_BLEND_ONE;
			BlendDestinationRGB[i] = BGFX_STATE_BLEND_ZERO;
			BlendSourceAlpha[i] = BGFX_STATE_BLEND_ONE;
			BlendDestinationAlpha[i] = BGFX_STATE_BLEND_ZERO;

			Blend[i] = false;

			for (u32 j = 0; j < 4; ++j)
				ColorMask[i][j] = true;

			CullFace[i] = false;
			CullFaceMode[i] = BGFX_STATE_CULL_CCW;
		}

		const core::dimension2d<u32> ScreenSize = Driver->getScreenSize();
		ViewportWidth = ScreenSize.Width;
		ViewportHeight = ScreenSize.Height;
		//BGFX set ViewRect for View 0 -->
		//TODO: add a view setting
		bgfx::setViewRect(0,ViewportX, ViewportY, ViewportWidth, ViewportHeight);
	}

	virtual ~CBgfxCoreCacheHandler()
	{
		delete[] BlendEquation;
		delete[] BlendFunc;
		delete[] BlendSourceRGB;
		delete[] BlendDestinationRGB;
		delete[] BlendSourceAlpha;
		delete[] BlendDestinationAlpha;
		delete[] Blend;

		delete[] ColorMask;

		delete[] CullFace;
		delete[] CullFaceMode;
	}

	E_DRIVER_TYPE getDriverType() const
	{
		return Driver->getDriverType();
	}

	STextureCache& getTextureCache()
	{
		return TextureCache;
	}

	virtual bgfxFlag_t getBgfxStateFlag(u32 viewIndx) const
	{
		if(FrameBufferCount <= viewIndx)
			return 0;

		return UINT64_C(0) | (DepthMask ? BGFX_STATE_DEPTH_WRITE : UINT64_C(0)) | (ColorMask[viewIndx][3]? BGFX_STATE_ALPHA_WRITE : UINT64_C(0))
				| (ColorMask[viewIndx][0]? BGFX_STATE_RGB_WRITE : UINT64_C(0)) | (Blend[viewIndx] ? (UINT64_C(0) | BlendEquation[viewIndx] | BlendFunc[viewIndx]) : UINT64_C(0)) //BlendEquation[viewIndx] | BlendFunc[viewIndx]
				| (CullFace[viewIndx] ? CullFaceMode[viewIndx] : UINT64_C(0)) | (DepthTest ? DepthFunc : UINT64_C(0)) | extraFlag;
	}

	void applyBgfxState(u32 viewIndx)
	{
		if(FrameBufferCount <= viewIndx)
			return;

		bgfx::setState(getBgfxStateFlag(viewIndx));

	}

	void addExtraFlag(bgfxFlag_t flag)
	{
		extraFlag |= flag;
	}

	void resetExtraFlags()
	{
		extraFlag = 0;
	}

	void removeExtraFlag(bgfxFlag_t flag)
	{
		extraFlag &= ~flag;
	}
	// Blending calls.

	void setBlendEquation(bgfxFlag_t mode)
	{
		BlendEquation[activeView] = BGFX_STATE_BLEND_EQUATION(mode);
	}

	void setBlendEquationIndexed(u32 index, bgfxFlag_t mode)
	{
		if (index < FrameBufferCount && BlendEquation[index] != mode)
		{
			BlendEquation[index] = BGFX_STATE_BLEND_EQUATION(mode);
		}
	}

	void setBlendEquationSeparate(bgfxFlag_t modeRGB, bgfxFlag_t modeA)
	{
		BlendEquation[activeView] = BGFX_STATE_BLEND_EQUATION_SEPARATE(modeRGB, modeA);
	}

	void setBlendEquationSeparateIndexed(u32 index, bgfxFlag_t modeRGB, bgfxFlag_t modeA)
	{
		if (index < FrameBufferCount && BlendEquation[index] != modeA)
		{
			BlendEquation[index] = BGFX_STATE_BLEND_EQUATION_SEPARATE(modeRGB, modeA);
		}
	}

	//set BlendFunc for current view
	void setBlendFunc(bgfxFlag_t source, bgfxFlag_t destination)
	{
		BlendSourceRGB[activeView] = source;
		BlendDestinationRGB[activeView] = destination;
		BlendSourceAlpha[activeView] = source;
		BlendDestinationAlpha[activeView] = destination;
		BlendFunc[activeView]=BGFX_STATE_BLEND_FUNC(source,destination);
	}
	//set BlendFunc for the current view
	void setBlendFuncSeparate(bgfxFlag_t sourceRGB, bgfxFlag_t destinationRGB, bgfxFlag_t sourceAlpha, bgfxFlag_t destinationAlpha)
	{
		BlendSourceRGB[activeView] = sourceRGB;
		BlendDestinationRGB[activeView] = destinationRGB;
		BlendSourceAlpha[activeView] = sourceAlpha;
		BlendDestinationAlpha[activeView] = destinationAlpha;
		BlendFunc[activeView]=BGFX_STATE_BLEND_FUNC_SEPARATE(sourceRGB,destinationRGB,sourceAlpha,destinationAlpha);
	}

	void setBlendFuncIndexed(u32 index, bgfxFlag_t source, bgfxFlag_t destination)
	{
		BlendSourceRGB[index] = source;
		BlendDestinationRGB[index] = destination;
		BlendSourceAlpha[index] = source;
		BlendDestinationAlpha[index] = destination;
		BlendFunc[index]=BGFX_STATE_BLEND_FUNC(source,destination);
	}

	void setBlendFuncSeparateIndexed(u32 index, bgfxFlag_t sourceRGB, bgfxFlag_t destinationRGB, bgfxFlag_t sourceAlpha, bgfxFlag_t destinationAlpha)
	{
		BlendSourceRGB[index] = sourceRGB;
		BlendDestinationRGB[index] = destinationRGB;
		BlendSourceAlpha[index] = sourceAlpha;
		BlendDestinationAlpha[index] = destinationAlpha;
		BlendFunc[index]=BGFX_STATE_BLEND_FUNC_SEPARATE(sourceRGB,destinationRGB,sourceAlpha,destinationAlpha);
	}


	void setBlend(bool enable)
	{
		Blend[activeView] = enable;
	}

	void setBlendIndexed(u32 index, bool enable)
	{
		if (index < FrameBufferCount)
		{
			Blend[index] = enable;
		}
	}

	// Color Mask.
	//Until now we can only turn on/off alpha / rgb
	void setColorMask(bool red, bool green, bool blue, bool alpha)
	{
		ColorMask[activeView][0] = red;
		ColorMask[activeView][1] = green;
		ColorMask[activeView][2] = blue;
		ColorMask[activeView][3] = alpha;
	}

	void setColorMaskIndexed(u32 index, bool red, bool green, bool blue, bool alpha)
	{
		if (index < FrameBufferCount)
		{
			ColorMask[index][0] = red;
			ColorMask[index][1] = green;
			ColorMask[index][2] = blue;
			ColorMask[index][3] = alpha;
		}
	}

	// Cull face calls.

	void setCullFaceFunc(bgfxFlag_t mode)
	{
		CullFaceMode[activeView] = mode;
	}

	void setCullFace(bool enable)
	{
		CullFace[activeView] = enable;
	}

	void setCullFaceFunc(u32 viewIdx, bgfxFlag_t mode)
	{
		CullFaceMode[viewIdx] = mode;
	}

	void setCullFace(u32 viewIdx, bool enable)
	{
		CullFace[viewIdx] = enable;
	}
	// Depth calls.

	void setDepthFunc(uint64_t mode)
	{
		DepthFunc = mode;
	}

	void setDepthMask(bool enable)
	{
		DepthMask = enable;
	}

	void setDepthTest(bool enable)
	{
		DepthTest = enable;
	}

	// FBO calls.

	void getFBO(u32& frameBufferID) const
	{
		frameBufferID = FrameBufferID;
	}

	void setFBO(u32 frameBufferID)
	{
		/*std::cerr << "CBgfxCoreCacheHandler::setFBO FBOHandle.idx(" << FBOHandle.idx << ") -> FrameBufferID(" << frameBufferID \
		<< ") FrameBufferID(" << FrameBufferID << ") -> frameBufferID(" << frameBufferID << std::endl;*/
		if (FrameBufferID != frameBufferID)
		{
			FBOHandle.idx = frameBufferID;
			FrameBufferID = frameBufferID;
		}
	}

    u32 getNextRenderTargetView() const
    {
        return this->nextRenderTargetView;
    }

    void setNextRenderTargetView(const u32 viewID)
    {
        this->nextRenderTargetView = viewID;
    }

	// Shaders calls.

	void getProgram(u32& programID) const
	{
		programID = ProgramID;
	}

	void setProgram(u32 programID)
	{
		if (ProgramID != programID)
		{
			programHandle.idx = programID;
			ProgramID = programID;
		}
	}

	// Texture calls.

	inline void getActiveTexture(u32& texture) const
	{
		texture = ActiveTexture;
	}

	inline void setActiveTexture(u32 texture)
	{
		ActiveTexture = texture;
	}

	// Viewport calls.

	void getViewport(s32& viewportX, s32& viewportY, s32& viewportWidth, s32& viewportHeight) const
	{
		viewportX = ViewportX;
		viewportY = ViewportY;
		viewportWidth = ViewportWidth;
		viewportHeight = ViewportHeight;
	}

	void setViewport(s32 viewportX, s32 viewportY, s32 viewportWidth, s32 viewportHeight)
	{
        // always setting the viewport because bgfx has a viewPort for every view and internally keeps track of what has been set -> no need to keep track on our own
		//std::cerr << "CBgfxCoreCacheHandler::setViewport comparing viewport for view(" << static_cast<int>(activeView) << "): (" << ViewportX << ", " << ViewportY << ", " << ViewportWidth << ", " << ViewportHeight << ")" << std::endl;
		//if (ViewportX != viewportX || ViewportY != viewportY || ViewportWidth != viewportWidth || ViewportHeight != viewportHeight)
		{
			ViewportX = viewportX;
			ViewportY = viewportY;
			ViewportWidth = viewportWidth;
			ViewportHeight = viewportHeight;
			//BGFX set ViewRect for View 0 -->
			//TODO: add a view setting
			//std::cerr << "CBgfxCoreCacheHandler::setViewport for view(" << static_cast<int>(activeView) << ") to (" << ViewportX << ", " << ViewportY << ", " << ViewportWidth << ", " << ViewportHeight << ")" << std::endl;
			bgfx::setViewRect(activeView, ViewportX, ViewportY, ViewportWidth, ViewportHeight);
		}
	}

	s32 addShaderConstant(const core::stringc name, const u32 size) {
		// check if this constant already exists
		s32 index = this->getShaderConstantID(name);
		if (index != -1) {
			return index;
		}

		SUniformInfo newUniform;
		newUniform.name = name;
		newUniform.num = 1;

		constexpr float vec4[4] = {1, 2, 3, 4};

		switch (size) {
            case 4:
                newUniform.handle = bgfx::createUniform(newUniform.name.c_str(), bgfx::UniformType::Vec4);
                bgfx::setUniform(newUniform.handle, vec4);
                break;
            case 9:
                newUniform.handle = bgfx::createUniform(newUniform.name.c_str(), bgfx::UniformType::Mat3);
                break;
            case 16:
                newUniform.handle = bgfx::createUniform(newUniform.name.c_str(), bgfx::UniformType::Mat4);
                break;
            default:
				if (size % 4 == 0)
				{
					newUniform.handle = bgfx::createUniform(newUniform.name.c_str(), bgfx::UniformType::Vec4,size/4);
					newUniform.num = size / 4;
					break;
				}
				else
				{
					os::Printer::log((core::stringc("Error: The bgfx driver can't create a shader constant of size ") + core::stringc(size) + core::stringc(" allowed sizes are 4, 9, 16 and other multiples of 4.")).c_str(), ELL_ERROR);
					return -1;
				}
                
        }

		this->UniformInfo.push_back(newUniform);
		index = this->UniformInfo.size() - 1;

        return index;
	}

    /** \brief get the index into the UniformInfo array for a certain uniform name.
     *
     * \param name core::stringc
     * \return s32. Index into the UniformInfo array. -1 if no uniform with that particular name found
     *
     */
	s32 getShaderConstantID(const core::stringc name) {
		for (u32 i = 0; i < this->UniformInfo.size(); i++) {
			if (this->UniformInfo[i].name == name) {
				return i;
			}
		}
		return -1;
	}

    /** \brief
     *
     * \param index const u32 index of the uniform. Use getShaderConstantID to get the id
     * \return void
     *
     */
	void addShaderConstantUser(const u32 index) {
		this->UniformInfo[index].UsedBy += 1;
	}

    /** \brief removes a user of a certain uniform. If it is used by noone else it will be deleted. This shifts all indices of the other uniforms!
     *
     * \param index const u32
     * \return void
     *
     */
     //TODO: find out if get*ShaderConstantID should return a constant ID or if the ID is allowed to change
	void removeShaderConstantUser(const u32 index) {
		this->UniformInfo[index].UsedBy -= 1;
		if (this->UniformInfo[index].UsedBy <= 0) {
			bgfx::destroy(UniformInfo[index].handle);
			this->UniformInfo.erase(index);
		}
	}

	struct SUniformInfo
    {
        core::stringc name;
        bgfx::UniformHandle handle = BGFX_INVALID_HANDLE;
		uint16_t num; //number of (vec4,mat3,mat4) entries 
        s32 UsedBy = 0;
    };

    core::array<SUniformInfo> UniformInfo;

protected:
	TBgfxDriver* Driver;

	STextureCache TextureCache;

	u32 FrameBufferCount;

	bgfxFlag_t* BlendEquation;
	bgfxFlag_t* BlendFunc;
	bgfxFlag_t* BlendSourceRGB;
	bgfxFlag_t* BlendDestinationRGB;
	bgfxFlag_t* BlendSourceAlpha;
	bgfxFlag_t* BlendDestinationAlpha;
	bool* Blend;

	bool(*ColorMask)[4];

	bgfxFlag_t* CullFaceMode;
	bool* CullFace;

	bgfxFlag_t DepthFunc;
	bool DepthMask;
	bool DepthTest;

	u32 FrameBufferID;
	bgfx::FrameBufferHandle FBOHandle;

	u32 ProgramID;
	bgfx::ProgramHandle programHandle;

	u32 ActiveTexture;

	s32 ViewportX;
	s32 ViewportY;
	s32 ViewportWidth;
	s32 ViewportHeight;

	bgfx::ViewId reservedViews = 12;
	bgfx::ViewId activeView = bgfx::getCaps()->limits.maxViews - 11; // default: render all renderTargets first, then everything not to be rendered to a framebuffer
	bgfx::ViewId prevView = bgfx::getCaps()->limits.maxViews - 11;
	bgfx::ViewId GuiView = bgfx::getCaps()->limits.maxViews - 10; // and 2d rendering done last (views are rendered sequentially)
    // each renderTarget is rendered onto a framebuffer. This framebuffer is bound to the active view in RenderTarget->update(). bgfx renders views in successive order.
    // the user expects successive setRenderTarget calls to render after one another (e.g. during post-processing).
    // to ensure this behaviour setRenderTarget() uses this counter to get the view the next renderTarget needs to be rendered to.
    // this counter should be reset in VideoDriver::beginScene() to make sure we don't run out of views
    bgfx::ViewId nextRenderTargetView = 0;
	
	bgfxFlag_t extraFlag = 0;
};

}
}

#endif //#ifdef _IRR_COMPILE_WITH_BGFX_

#endif
