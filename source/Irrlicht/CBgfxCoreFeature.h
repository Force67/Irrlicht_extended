// Copyright (C) 2015 Patryk Nadrowski
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BgfxCORE_FEATURE_H_INCLUDED__
#define __C_BgfxCORE_FEATURE_H_INCLUDED__

#include "IrrCompileConfig.h"

#include "irrTypes.h"

namespace irr
{
namespace video
{

class CBgfxCoreFeature
{
public:
	CBgfxCoreFeature() : BlendOperation(false), ColorAttachment(0), MultipleRenderTarget(0), TextureUnit(1)
	{
	}

	virtual ~CBgfxCoreFeature()
	{
	}

	bool BlendOperation;

	u8 ColorAttachment;
	u16 MultipleRenderTarget;
	u8 TextureUnit;
	u32 MaxViews;
};

}
}

#endif
