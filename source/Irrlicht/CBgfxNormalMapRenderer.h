// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BGFX_NORMAL_MAP_RENDERER_H_INCLUDED__
#define __C_BGFX_NORMAL_MAP_RENDERER_H_INCLUDED__

#include "IrrCompileConfig.h"

#ifdef _IRR_COMPILE_WITH_BGFX_

#include "IShaderConstantSetCallBack.h"

#include "CBgfxShaderMaterialRenderer.h"

namespace irr
{
namespace video
{

//! Class for rendering normal maps with OpenGL
class CBgfxNormalMapRenderer : public CBgfxShaderMaterialRenderer, public IShaderConstantSetCallBack
{
public:

	//! Constructor
	CBgfxNormalMapRenderer(video::CBgfxDriver* driver,
		s32& outMaterialTypeNr, E_MATERIAL_TYPE baseMaterial);

	//! Destructor
	~CBgfxNormalMapRenderer();

	//! Called by the engine when the vertex and/or pixel shader constants for an
	//! material renderer should be set.
	virtual void OnSetConstants(IMaterialRendererServices* services, s32 userData) _IRR_OVERRIDE_;

	//! Returns the render capability of the material.
	virtual s32 getRenderCapability() const _IRR_OVERRIDE_;

protected:

	bool CompiledShaders;
};


} // end namespace video
} // end namespace irr

#endif //#ifdef _IRR_COMPILE_WITH_BGFX_

#endif
