// Copyright (C) 2002-2012 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CBgfxShaderMaterialRenderer.h"

#ifdef _IRR_COMPILE_WITH_BGFX_

#include "IGPUProgrammingServices.h"
#include "IShaderConstantSetCallBack.h"
#include "IVideoDriver.h"
#include "os.h"

#include "CBgfxDriver.h"
#include "CBgfxCacheHandler.h"
#include "CBgfxMaterialRenderer.h"

namespace irr
{
namespace video
{


//! Constructor
CBgfxShaderMaterialRenderer::CBgfxShaderMaterialRenderer(video::CBgfxDriver* driver, s32& outMaterialTypeNr,
        const void* vertexShaderProgram, const u32 vertexShaderProgramSize,
        const void* pixelShaderProgram, const u32 pixelShaderProgramSize,
	IShaderConstantSetCallBack* callback, E_MATERIAL_TYPE baseMaterial, s32 userData, const core::array<core::stringc> constantNames, const core::array<u32> constantSizes)
	:  CBgfxMaterialRenderer_BASE(driver), CallBack(callback), Alpha(false), Blending(false), FixedBlending(false),
		AlphaTest(false), UserData(userData)
{
	#ifdef _DEBUG
	setDebugName("CBgfxShaderMaterialRenderer");
	#endif

	switch (baseMaterial)
	{
	case EMT_TRANSPARENT_VERTEX_ALPHA:
	case EMT_TRANSPARENT_ALPHA_CHANNEL:
	case EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA:
	case EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA:
		Alpha = true;
		break;
	case EMT_TRANSPARENT_ADD_COLOR:
	case EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR:
	case EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR:
		FixedBlending = true;
		break;
	case EMT_ONETEXTURE_BLEND:
		Blending = true;
		break;
	case EMT_TRANSPARENT_ALPHA_CHANNEL_REF:
		AlphaTest = true;
		break;
	default:
		break;
	}

	if (CallBack)
		CallBack->grab();

    outMaterialTypeNr = -1;

    if (constantNames.size() != constantSizes.size()) {
        os::Printer::log("Error: constantNames and constantSizes have different size!", ELL_ERROR);
    } else {
        this->createShaderUniforms(constantNames, constantSizes);
        init(outMaterialTypeNr, vertexShaderProgram, vertexShaderProgramSize, pixelShaderProgram, pixelShaderProgramSize, EVT_STANDARD);
    }
}


//! constructor only for use by derived classes who want to
//! create a fall back material for example.
CBgfxShaderMaterialRenderer::CBgfxShaderMaterialRenderer(CBgfxDriver* driver,
				IShaderConstantSetCallBack* callback,
				E_MATERIAL_TYPE baseMaterial, s32 userData)
: CBgfxMaterialRenderer_BASE(driver), CallBack(callback), Alpha(false), Blending(false), FixedBlending(false),
	AlphaTest(false), UserData(userData)
{
	switch (baseMaterial)
	{
	case EMT_TRANSPARENT_VERTEX_ALPHA:
	case EMT_TRANSPARENT_ALPHA_CHANNEL:
	case EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA:
	case EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA:
		Alpha = true;
		break;
	case EMT_TRANSPARENT_ADD_COLOR:
	case EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR:
	case EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR:
		FixedBlending = true;
		break;
	case EMT_ONETEXTURE_BLEND:
		Blending = true;
		break;
	case EMT_TRANSPARENT_ALPHA_CHANNEL_REF:
		AlphaTest = true;
		break;
	default:
		break;
	}

	if (CallBack)
		CallBack->grab();
}


//! Destructor
CBgfxShaderMaterialRenderer::~CBgfxShaderMaterialRenderer()
{
	if (CallBack)
		CallBack->drop();

    if (bgfx::isValid(this->CompiledShader)) {
        bgfx::destroy(this->CompiledShader);
    }

    for (u32 i = 0; i < this->UsedUniforms.size(); i++) {
        s32 id = this->Driver->getCacheHandler()->getShaderConstantID(this->UsedUniforms[i]);
        if (id < 0) {
            std::cerr << "CBgfxShaderMaterialRenderer: this shouldn't happen: used a uniform that wasn't set in the CacheHandler!!!" << std::endl;
            std::exit(1);
        }
        this->Driver->getCacheHandler()->removeShaderConstantUser(id);
    }
}


void CBgfxShaderMaterialRenderer::init(s32& outMaterialTypeNr,
		const void* vertexShaderProgram, const u32 vertexShaderProgramSize,
        const void* pixelShaderProgram, const u32 pixelShaderProgramSize,
		E_VERTEX_TYPE type)
{
    _IRR_DEBUG_BREAK_IF(sizeof(c8) != sizeof(uint8_t));

    if (pixelShaderProgram != nullptr) {
        if (!this->compileShaders(static_cast<const uint8_t*>(vertexShaderProgram), vertexShaderProgramSize, static_cast<const uint8_t*>(pixelShaderProgram), pixelShaderProgramSize))
        {
            return;
        }
    }
    else {
        std::cerr << "can't compile only vertex shader!" << std::endl;
        std::exit(2);
        if (!this->compileShaders(static_cast<const uint8_t*>(vertexShaderProgram), vertexShaderProgramSize)) {
            return;
        }
    }

    //std::cerr << "adding material renderer at " << static_cast<void*>(this) << std::endl;

	// register as a new material
	outMaterialTypeNr = Driver->addMaterialRenderer(this);
}

void CBgfxShaderMaterialRenderer::createShaderUniforms(const core::array<core::stringc> constantNames, const core::array<u32> constantSizes)
{
    for (u32 i = 0; i < constantNames.size(); i++) {
        const s32 idx = this->Driver->getCacheHandler()->addShaderConstant(constantNames[i], constantSizes[i]);
        if (idx != -1) { // constant creation might fail
            this->Driver->getCacheHandler()->addShaderConstantUser(idx);
            this->UsedUniforms.push_back(constantNames[i]);
        }
    }
}


bool CBgfxShaderMaterialRenderer::OnRender(IMaterialRendererServices* service, E_VERTEX_TYPE vtxtype)
{
	// call callback to set shader constants
	if (CallBack && bgfx::isValid(this->CompiledShader))
		CallBack->OnSetConstants(this, UserData);

	return true;
}

IVideoDriver* CBgfxShaderMaterialRenderer::getVideoDriver() {
    return this->Driver;
}


void CBgfxShaderMaterialRenderer::OnSetMaterial(const video::SMaterial& material, const video::SMaterial& lastMaterial,
	bool resetAllRenderstates, video::IMaterialRendererServices* services)
{
/*	if (Driver->getFixedPipelineState() == CBgfxDriver::EOFPS_ENABLE)
		Driver->setFixedPipelineState(CBgfxDriver::EOFPS_ENABLE_TO_DISABLE);
	else
		Driver->setFixedPipelineState(CBgfxDriver::EOFPS_DISABLE);*/

	CBgfxCacheHandler* cacheHandler = Driver->getCacheHandler();

	Driver->setBasicRenderStates(material, lastMaterial, resetAllRenderstates);

	if (Alpha)
	{
		cacheHandler->setBlend(true);
		cacheHandler->setBlendFunc(BGFX_STATE_BLEND_SRC_ALPHA, BGFX_STATE_BLEND_INV_SRC_ALPHA);
	}
	else if (FixedBlending)
	{
		cacheHandler->setBlendFunc(BGFX_STATE_BLEND_ONE, BGFX_STATE_BLEND_INV_SRC_COLOR);
		cacheHandler->setBlend(true);
	}
	else if (Blending)
	{
		E_BLEND_FACTOR srcRGBFact,dstRGBFact,srcAlphaFact,dstAlphaFact;
		E_MODULATE_FUNC modulate;
		u32 alphaSource;
		unpack_textureBlendFuncSeparate(srcRGBFact, dstRGBFact, srcAlphaFact, dstAlphaFact, modulate, alphaSource, material.MaterialTypeParam);

		if (Driver->queryFeature(EVDF_BLEND_SEPARATE))
        {
			cacheHandler->setBlendFuncSeparate(Driver->getBgfxBlend(srcRGBFact), Driver->getBgfxBlend(dstRGBFact),
                Driver->getBgfxBlend(srcAlphaFact), Driver->getBgfxBlend(dstAlphaFact));
        }
        else
        {
			cacheHandler->setBlendFunc(Driver->getBgfxBlend(srcRGBFact), Driver->getBgfxBlend(dstRGBFact));
        }

		cacheHandler->setBlend(true);
	}
	else if (AlphaTest)
	{
		cacheHandler->setAlphaTest(true);
		cacheHandler->setAlphaFunc(true, 0.5f);
	}

	if (CallBack)
		CallBack->OnSetMaterial(material);
}


void CBgfxShaderMaterialRenderer::OnUnsetMaterial()
{
	// disable vertex shader
/*#ifdef GL_ARB_vertex_program
	if (VertexShader)
		glDisable(GL_VERTEX_PROGRAM_ARB);
#elif defined(GL_NV_vertex_program)
	if (VertexShader)
		glDisable(GL_VERTEX_PROGRAM_NV);
#endif

#ifdef GL_ARB_fragment_program
	if (PixelShader[0])
		glDisable(GL_FRAGMENT_PROGRAM_ARB);
#elif defined(GL_NV_fragment_program)
	if (PixelShader[0])
		glDisable(GL_FRAGMENT_PROGRAM_NV);
#endif*/
}


//! Returns if the material is transparent.
bool CBgfxShaderMaterialRenderer::isTransparent() const
{
	return (Alpha || Blending || FixedBlending);
}


// implementations for the render services
void CBgfxShaderMaterialRenderer::setBasicRenderStates(const SMaterial& material,
						const SMaterial& lastMaterial,
						bool resetAllRenderstates)
{
	// forward
	this->Driver->setBasicRenderStates(material, lastMaterial, resetAllRenderstates);
}

s32 CBgfxShaderMaterialRenderer::getVertexShaderConstantID(const c8* name) {
    return this->getPixelShaderConstantID(name);
}

s32 CBgfxShaderMaterialRenderer::getPixelShaderConstantID(const c8* name) {
    const s32 id = this->Driver->getCacheHandler()->getShaderConstantID(name);
    if (id == -1 || (!bgfx::isValid(this->Driver->getCacheHandler()->UniformInfo[id].handle))) {
        // bgfx needs the uniforms to be created before the first submit, even if the shader isn't currently used.
        os::Printer::log((core::stringc("Error: bgfx Driver: shader constant '") + core::stringc(name) + \
                          core::stringc("' wasn't created. Make sure it is in 'constantNames' when calling VideoDriver->addHighLevelShaderMaterial!")).c_str(), ELL_ERROR);
        return -1;
    }
    return id;
}

void CBgfxShaderMaterialRenderer::setVertexShaderConstant(const f32* data, s32 startRegister, s32 constantAmount)
{
	os::Printer::log("Cannot set constant, please use high level shader call instead.", ELL_WARNING);
}

void CBgfxShaderMaterialRenderer::setPixelShaderConstant(const f32* data, s32 startRegister, s32 constantAmount)
{
	os::Printer::log("Cannot set constant, use high level shader call.", ELL_WARNING);
}

bool CBgfxShaderMaterialRenderer::setVertexShaderConstant(s32 index, const f32* floats, int count) {
    return this->setPixelShaderConstant(index, floats, count);
}

bool CBgfxShaderMaterialRenderer::setVertexShaderConstant(s32 index, const s32* ints, int count) {
    os::Printer::log("Cannot set constant, use high level shader call.", ELL_WARNING);
    return false;
}

bool CBgfxShaderMaterialRenderer::setPixelShaderConstant(s32 index, const f32* floats, int count) {
    if (index < 0 || index + 1 > this->Driver->getCacheHandler()->UniformInfo.size()) {
        return false;
    }

    CBgfxCacheHandler::SUniformInfo& uniform = this->Driver->getCacheHandler()->UniformInfo[index];
    if (!bgfx::isValid(uniform.handle)) {
        os::Printer::log((core::stringc("Error: bgfx Driver: shader constant with id '") + core::stringc(index) + \
                          core::stringc("' wasn't created. Make sure it is in 'constantNames' when calling VideoDriver->addHighLevelShaderMaterial!")).c_str(), ELL_ERROR);
        return false;
    }

//    std::cerr << "setting " << count << " values for uniform[" << index << "] = " << uniform.handle.idx << " with name " << uniform.name.c_str() << " first value = " << floats[0] << std::endl;
    bgfx::setUniform(uniform.handle, floats,uniform.num);
    //std::cerr << "done setting!" << std::endl;
    return true;
}

bool CBgfxShaderMaterialRenderer::setPixelShaderConstant(s32 index, const s32* ints, int count) {
    return false;
}

} // end namespace video
} // end namespace irr

#endif //#ifdef _IRR_COMPILE_WITH_BGFX_
