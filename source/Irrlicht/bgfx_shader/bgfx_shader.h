///:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
///GLSL
///:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//Basic Shader - no texcoords
#include "basic_vs_glsl_bgfx.h"
#include "basic_fs_glsl_bgfx.h"

#include "standard_vs_glsl_bgfx.h"
#include "standard_fs_glsl_bgfx.h"

#include "onetextureblend_vs_glsl_bgfx.h"
#include "onetextureblend_fs_glsl_bgfx.h"

#include "transparentAddColor_vs_glsl_bgfx.h"
#include "transparentAddColor_fs_glsl_bgfx.h"

///:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
///HLSL DX9 - and DX11
///:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include "basic_vs_hlsl_DX9_bgfx.h"
#include "basic_fs_hlsl_DX9_bgfx.h"

#include "basic_vs_hlsl_DX11_bgfx.h"
#include "basic_fs_hlsl_DX11_bgfx.h"

#include "standard_vs_hlsl_DX9_bgfx.h"
#include "standard_fs_hlsl_DX9_bgfx.h"

#include "standard_vs_hlsl_DX11_bgfx.h"
#include "standard_fs_hlsl_DX11_bgfx.h"

#include "onetextureblend_vs_hlsl_DX9_bgfx.h"
#include "onetextureblend_fs_hlsl_DX9_bgfx.h"

#include "onetextureblend_vs_hlsl_DX11_bgfx.h"
#include "onetextureblend_fs_hlsl_DX11_bgfx.h"

#include "transparentAddColor_vs_hlsl_DX9_bgfx.h"
#include "transparentAddColor_fs_hlsl_DX9_bgfx.h"

#include "transparentAddColor_vs_hlsl_DX11_bgfx.h"
#include "transparentAddColor_fs_hlsl_DX11_bgfx.h"
