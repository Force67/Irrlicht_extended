$input a_position, a_normal, a_color0, a_texcoord0
$output v_color0, v_texcoord0, v_normal


#include "common.sh"

void main()
{
	gl_Position = mul(u_modelViewProj, vec4(a_position, 1.0) );
	v_color0 = a_color0.bgra;
	v_texcoord0 = a_texcoord0;
	
	vec4 normal = vec4(a_normal * 2.0 - 1.0,1.0);
	vec3 wnormal = mul(u_model[0], vec4(normal.xyz, 0.0) ).xyz;
	vec3 viewNormal = normalize(mul(u_view, vec4(wnormal, 0.0) ).xyz);
	v_normal = viewNormal;	
}
