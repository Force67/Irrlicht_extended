OS=$1
SHADERC=$2
BINTOC=$3
OS=Windows_NT
PLATFORM="linux"
SHADER_LANGUAGE="glsl"
if [ "x$OS" = "xWindows_NT" ]; then
    echo "building hlsl and glsl shaders on windows!"
    PLATFORM="windows"
fi
SHADERC=./shadercRelease.exe
if [ -z "$SHADERC" ]; then
    echo "" >&2
    echo "ERROR:" >&2
    echo "\$SHADERC variable not set!" >&2
    echo "Add a line 'SHADERC := path/to/bgfx/shaderc/tool' to your userlibs.mk" >&2
    echo "" >&2
    exit 1
fi

for SHADER_TYPE in v f; do
	PROFILE="140" # use glsl v1.30
    
	for SHADER_FILE in $(find . -name '*_'$SHADER_TYPE's.sc'); do
        echo "compiling shader file to type glsl = '$SHADER_FILE'"
        TARGET_FILE="$(dirname "$SHADER_FILE")"/"$(basename --suffix=.sc "$SHADER_FILE")"_"$SHADER_LANGUAGE"_bgfx.h
        $SHADERC --profile "$PROFILE" -f "$SHADER_FILE" -o "$TARGET_FILE" --type $SHADER_TYPE --platform $PLATFORM -O 3 --bin2c $(basename --suffix=.sc "$SHADER_FILE")_"$SHADER_LANGUAGE"_bgfx || exit 2
    done
	
	if [ "x$PLATFORM" = "xwindows" ]; then
		SHADER_LANGUAGE="hlsl"
		if [ "x$SHADER_TYPE" = "xv" ]; then
			PROFILE="vs_4_0" # use hlsl vertex shader v3.0
		else
			PROFILE="ps_4_0" # use hlsl pixel shader v3.0
		fi
		
		for SHADER_FILE in $(find . -name '*_'$SHADER_TYPE's.sc'); do
			echo "compiling shader file to type hlsl = '$SHADER_FILE'"
			TARGET_FILE="$(dirname "$SHADER_FILE")"/"$(basename --suffix=.sc "$SHADER_FILE")"_"$SHADER_LANGUAGE"_bgfx.h
			$SHADERC --profile "$PROFILE" -f "$SHADER_FILE" -o "$TARGET_FILE" --type $SHADER_TYPE --platform $PLATFORM -O 3 --bin2c $(basename --suffix=.sc "$SHADER_FILE")_"$SHADER_LANGUAGE"_bgfx || exit 2
		done
		PROFILE="140"
		SHADER_LANGUAGE="glsl"
	fi
done
